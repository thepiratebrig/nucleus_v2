/*neuron_associative.cpp by ya_sayanb from TPBN*/
#include "neuron_associative.h"



neuron_associative::neuron_associative(
	typeInt myNumberOfInputs, typeFloat* myWeigers)
{
	//���-�� ������ �������
	numberOfInputs = myNumberOfInputs;
	//������� ���-��(��� ���� �� ���)
	curNumOfInputs = 0;
	//����
	weigers = new typeFloat[numberOfInputs];
	//��������� �� �����
	arrayPointersNeurons = new basic_neuron*[numberOfInputs];
	//������ �����
	for (typeInt i = 0; i < numberOfInputs; i++) 
		weigers[i] = myWeigers[i];
}


neuron_associative::~neuron_associative()
{
	//������� 
	if (weigers) {
		delete[]weigers;
		delete[]arrayPointersNeurons;
		weigers = 0;
		arrayPointersNeurons = 0;
		numberOfInputs = 0;
		curNumOfInputs = 0;
	}
}

void neuron_associative::processingFunction(typeFloat data)
{
	//����������� ���������
	typeFloat buf = sin(data);
	setResult(buf);
}

void neuron_associative::readData(typeInt isReceptor)
{
	typeFloat buf = 0;
	//������� ����� ������� ������
	if (isReceptor)
		for (int i = 0; i < numberOfInputs; i++)
			buf += weigers[i] * ((neuron_receptor*)
				arrayPointersNeurons[i])->getResult();
	else
		for (int i = 0; i < numberOfInputs; i++)
			buf += weigers[i] * ((neuron_associative*)
				arrayPointersNeurons[i])->getResult();
	
	processingFunction(buf);
}

void neuron_associative::getPointerNeuron(
	basic_neuron* pNeuron)
{
	//�������� ��������� �� ����
	arrayPointersNeurons[curNumOfInputs] = pNeuron;
	curNumOfInputs++;
}