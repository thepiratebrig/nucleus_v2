/*neural_network.cpp by ya_sayanb from TPBN*/
#include "neural_network.h"

std::ifstream fins;
std::ifstream finw;


neural_network::neural_network()
{
	//������ �� �����
	fins.open("settings.txt");
	
	if (fins.is_open()) {
		fins >> depth;//depth = 3;//���-�� �����(���. ���������)
		width = new typeInt[depth];
		for (typeInt i = 0; i < depth; i++)
			fins >> width[i];//���-�� �������� � ������ ����

		fins >> numberOfWeigers;//���-�� �����
		weigers = new typeFloat[numberOfWeigers];

		if (readWeigers("weigers.txt")) {

			//�������� �������
			{
				matrixOfPointersNeurons = new basic_neuron**[depth];
				//�������� ���� ����������
				{
					matrixOfPointersNeurons[0] =
						new basic_neuron*[width[0]];
					//�������� ����������
					for (int i = 0; i < width[0]; i++)
						matrixOfPointersNeurons[0][i] = new
						neuron_receptor;
				}
				//�������� ������������� ��������
				{
					for (int j = 1; j < depth; j++) {
						//�������� j-�� ���� ��������
						matrixOfPointersNeurons[j] =
							new basic_neuron*[width[j]];
						//�������� ��������
						for (int i = 0; i < width[j]; i++)
							matrixOfPointersNeurons[j][i] =
							new neuron_associative(numberOfWeigers, weigers);
					}
				}
				//�������� ������ ����� ���������
				createRelations();
			}
		}
		fins.close();
	}
}

neural_network::~neural_network()
{
	//�������� �������
	if (matrixOfPointersNeurons) {
		//�������� ����������
		for (int i = 0; i < width[0]; i++)
			delete ((neuron_receptor*)matrixOfPointersNeurons[0][i]);
		//�������� ���� ����������
		delete[] matrixOfPointersNeurons[0];
		//�������� ������������� ��������
		for (int j = 1; j < depth; j++) {
			//�������� ��������
			for (int i = 0; i < width[j]; i++)
				delete ((neuron_associative*)matrixOfPointersNeurons[j][i]);
			//�������� j-�� ���� ��������
			delete[] matrixOfPointersNeurons[j];
		}
		delete[] matrixOfPointersNeurons;
		delete[] width;
		delete[] weigers;
		//������� ����������
		depth = 0;
		width = 0;
		weigers = 0;
		numberOfWeigers = 0;
		matrixOfPointersNeurons = 0;
	}
}

typeFloat neural_network::readFunction(typeInt ** ppPixels)
{
	
	//������ �������
	std::vector<std::thread> mythreads;

	//������ ������� �������////////////////////////�������
	std::vector<typeInt> threadnumber;
	//�������� ������� ������
	for (typeInt i = 0; i < 9; i++)
		threadnumber.push_back(i);
	//�������� �������
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++) {
			//�������� ������
			mythreads.push_back(
				std::thread(
					//processingFunction,//not worked
					threadfunction, (void*)this,
					std::ref(threadnumber[i * 3 + j]),
					std::ref(ppPixels[i][j])
					)
				);
		}
	//������������� ���� �������(����, ���� ��� �������� ��������)
	for (auto& t : mythreads)
		t.join();
	//������� ������� ������� �������
	threadnumber.clear();

	//�������� ���������
	typeFloat result=
		matrixOfPointersNeurons[depth-1][0]->getResult();
	//������� ���������� � ��������//�� �� ����
	clearResults();

	return result;
}

typeFloat neural_network::readDataNoThreads(typeInt ** ppPixels)
{
	//������� ������ ������ ��� ������������� �������
	//�����
	typeInt data;

	//��� ��������� ������ ������
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++) {
			data = ppPixels[i][j];
			((neuron_receptor*)
				matrixOfPointersNeurons[0][i * 3 + j])->
				readData(&data);
		}
	
	//������ ���� �������� �� ����������
	int i = 1;
	for (int j = 0; j < width[i]; j++)
		((neuron_associative*)
			matrixOfPointersNeurons[i][j])->
		readData(1);
	//����������� ���� �������� �� ��������
	for (int i = 2; i < depth; i++)
		for (int j = 0; j < width[i]; j++)
			((neuron_associative*)
				matrixOfPointersNeurons[i][j])->
			readData(0);

	
	//������� ���������
	typeFloat result =
		matrixOfPointersNeurons[depth - 1][0]->getResult();
	//������ ���������� ��������//���� �� �������
	clearResults();

	return result;
}

void neural_network::processingFunction(typeInt numberOfThread,
	typeInt data)
{
	//��������� �������
	int i = 0;
	//�������� �������� ������
	((neuron_receptor*)
		matrixOfPointersNeurons[i][numberOfThread])->
		readData(&data);

	i++;
	//������ ������� ����
	if (width[i] > numberOfThread)
		((neuron_associative*)
			matrixOfPointersNeurons[i][numberOfThread])->
		readData(1);//1, ������ ��� � ���� ����� � �����������
	i++;
	//������ ����������� �����
	while (width[i]>numberOfThread) {
		((neuron_associative*)
			matrixOfPointersNeurons[i][numberOfThread])->
			readData(0);//0, ������ ��� ����� � ���������
		i++;
	}
}

void neural_network::clearResults()
{
	//������� �������
	{
		//������� ���� ����������
		{
			//������� ����������
			for (int i = 0; i < width[0]; i++)
				if (!matrixOfPointersNeurons[0][i]->clearNeuron())
					break;		}
		//������� ������������� ��������
		{
			for (int j = 1; j < depth; j++) {
				//������� j-�� ���� ��������

				//������� ��������
				for (int i = 0; i < width[j]; i++)
					if (!matrixOfPointersNeurons[j][i]->clearNeuron())
						break;
			}
		}
	}
}

void threadfunction(void* pNetwork, typeInt numberOfThread,
	typeInt data)
{
	//��� �� ������� ����, ��� ��������������
	((neural_network*)pNetwork)->
		processingFunction(numberOfThread, data);//���� ������� ����))
}

int neural_network::readWeigers(char* fileName) {
	finw.open(fileName);
	if (finw.is_open())
	{
		for (typeInt i = 0; i < numberOfWeigers; i++)
			finw >> weigers[i];//���� ����
		finw.close();
		return 1;
	}
	else {
		return 0;
	}
}

void neural_network::createRelations() {
	for (int i = 1; i < depth; i++)
		for (int j = 0; j < width[i]; j++)
			for (int k = 0; k < numberOfWeigers; k++)
				(
					(neuron_associative*)
					matrixOfPointersNeurons[i][j]
					)
				->getPointerNeuron(
					matrixOfPointersNeurons[0]
					[(j + k) % width[i]]);
					//[j*numberOfWeigers + k]);
}