/*neuron_associative.h by ya_sayanb from TPBN*/
#pragma once
#ifndef NEURON_ASSOCIATIVE_H    // ���� ���  ��� �� ����������
#define NEURON_ASSOCIATIVE_H   // ���������� ���
#include "basic_neuron.h"
#include "neuron_receptor.h"
class neuron_associative :
	public basic_neuron
{
public:
	//�����������
	neuron_associative(typeInt myNumberOfInputs, 
		typeFloat* myWeigers);
	//����������
	~neuron_associative();
	//�������, ����������� resultOfFunction
	void processingFunction(typeFloat data);
	//�������, �������� ������ �� ������� ��������
	void readData(typeInt isReceptor);
	//�������, ������� ��������� �� ������� ������
	void getPointerNeuron(basic_neuron* pNeuron);
private:
	//���-�� ������ �������
	typeInt numberOfInputs;
	//������� ���-�� ������ �������
	typeInt curNumOfInputs;
	//��������� �� ������ ����� �� ������ ����
	typeFloat* weigers;
	//��������� �� ������ ���������� �� ������� ������
	basic_neuron** arrayPointersNeurons;
};
#endif NEURON_ASSOCIATIVE_H     // ����  ���  ��� ����������, �������� �� ����������

