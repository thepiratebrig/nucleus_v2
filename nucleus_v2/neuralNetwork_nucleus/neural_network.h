/*neural_network.h by ya_sayanb from TPBN*/
#pragma once
#ifndef NEURAL_NETWORK_H    // ���� ���  ��� �� ����������
#define NEURAL_NETWORK_H   // ���������� ��� 
#include <thread>
#include <vector>//��� ��������
#include <iostream>
#include <fstream>//��� ������ �������� � �����
#include "basic_neuron.h"
#include "neuron_receptor.h" 
#include "neuron_associative.h" 

//������� ������//�� ���� ������
void threadfunction(void* pNetwork, typeInt numberOfThread,
	typeInt data);

class neural_network
{
public:
	//�����������
	neural_network();
	//����������
	~neural_network();
	//������� ��� ������
	void processingFunction(typeInt numberOfThread,typeInt data);
	//������ ������, ������� ���������
	typeFloat readFunction(typeInt** ppPixels);
	//������� ����������� ����� ��������� ������
	void clearResults();
	//������ ������, ������� ���������, ��� ���������������
	typeFloat readDataNoThreads(typeInt ** ppPixels);
	//������ ���� �� �����
	int readWeigers(char* fileName);
private:
	//�������� ������ ����� ���������
	void createRelations();
	//������� ���������� �� �������
	basic_neuron** *matrixOfPointersNeurons;
	//������� ��������� ����(������� �����, ������ ���������)
	typeInt depth;
	//������ ��������� ����(������� �������� � i-�� ����)
	typeInt* width;
	//���-�� �����
	typeInt numberOfWeigers;
	//����
	typeFloat* weigers;
};
#endif NEURAL_NETWORK_H     // ����  ���  ��� ����������, �������� �� ����������