/*basic_neuron.cpp by ya_sayanb from TPBN*/
#include "basic_neuron.h"

basic_neuron::basic_neuron()
{
	//���� ������� �� ����, ������ ��� �����
	while (basic_neuron::clearNeuron())
		//������ � ��� �� ��������� �����
		std::this_thread::sleep_for
		(TimeDelay);
}


basic_neuron::~basic_neuron()
{
	//���� ������� �� ����, ������ ��� �����
	while (basic_neuron::clearNeuron())
		//������ � ��� �� ��������� �����
		std::this_thread::sleep_for
		(TimeDelay);
}

typeFloat basic_neuron::getResult() {
	//����� �� ��������� ��� ������
	bool ResAvail = false;
	//����� ��� ����������
	typeFloat buf;
	while (!isResultCreated)
		std::this_thread::sleep_for
		(TimeDelay);;
	//���������,���� ������� �������� �����
	isResultAvailable.lock_shared();
	buf = resultOfFunction;
	//������������ �������� ��������
	isResultAvailable.unlock_shared();
	return buf;
}

void basic_neuron::setResult(typeFloat data) {
	//���������,���� ������� ��� �����
	isResultAvailable.lock();
	isResultCreated = true;
	resultOfFunction = data;
	isResultAvailable.unlock();
}

int basic_neuron::clearNeuron()
{
	//�������� ����� �������
	//���� ����� ���� �� ���
	if (isResultAvailable.try_lock())
	{
		isResultCreated = false;
		resultOfFunction = 0;
		isResultAvailable.unlock();
		//���� ������� ����, false
		return 0;
	}
	//���� ������� �� ����, true
	return 1;
}
