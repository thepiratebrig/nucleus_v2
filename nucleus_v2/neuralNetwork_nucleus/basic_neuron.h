/*basic_neuron.h by ya_sayanb from TPBN*/
#pragma once
#ifndef BASIC_NEURON_H    // ���� ���  ��� �� ����������
#define BASIC_NEURON_H   // ���������� ���
/*��������� ���������� �����,
	� ������� ������������ basic_neuron.h, 
		����� �2504*/
//#include <mutex>
#include <shared_mutex>
#include <thread>
#include <chrono>
#define TimeDelay std::chrono::microseconds(100)
typedef int typeInt;//��� �������������
typedef float typeFloat;//��� ������������


class basic_neuron
{
public:
	//�����������
	basic_neuron();
	//����������
	~basic_neuron();
	//���������� �������� ���������� ����������� �������
	typeFloat getResult();
	//���������� data � resultOfFunction 
	void setResult(typeFloat data);
	//���������� �������(�������)
	int clearNeuron();
private:
	//https://ru.wikipedia.org/wiki/%D0%97%D0%B0%D0%B4%D0%B0%D1%87%D0%B0_%D0%BE_%D1%87%D0%B8%D1%82%D0%B0%D1%82%D0%B5%D0%BB%D1%8F%D1%85-%D0%BF%D0%B8%D1%81%D0%B0%D1%82%D0%B5%D0%BB%D1%8F%D1%85
	
	//������� �� ������ � ����������-���������
	std::shared_timed_mutex isResultAvailable;
	//������� ���������� ����������� ����������
	bool isResultCreated=false;
	//����������, �������� ��������� �������
	typeFloat resultOfFunction;
};
#endif BASIC_NEURON_H     // ����  ���  ��� ����������, �������� �� ����������
