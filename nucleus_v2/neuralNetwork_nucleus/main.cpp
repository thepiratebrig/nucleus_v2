/*main.cpp by ya_sayanb from TPBN*/
#include "main.h"

using namespace std;
ifstream fin;
ofstream fout;



int main() {
	//�������� ������� ������
	{
		ii[0] = i0;
		ii[1] = i45;
		ii[2] = i90;
		ii[3] = i135;
		oo[0] = o0;
		oo[1] = o45;
		oo[2] = o90;
		oo[3] = o135;
	}
	
	processingData();

	cout << ">For exit press any key" << endl;
	cin.get();
	return 0;
}

void processingData() 
{
	neural_network myNetwork;//���������
	typeFloat result;//����� �����������
	int** ppPixels = new int*[3];//������� ��� ������
	for (int i = 0; i < 3; i++)
		ppPixels[i] = new int[3];//������ �������
	
	for (int n = 0; n < 4; n++)//��� 4 �����
	{

		cout << ">Open files..." << endl;
		fin.open(ii[n]);
		fout.open(oo[n]);

		if (fin.is_open() && fout.is_open()) {
			cout << ">Files opened!" << endl;
			//������
			int x;
			fin >> x;
			for (int i = 0; i < x; i++) {
				//������
				cout << ">Read test No" << i << endl;
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 3; j++) {
						fin >> ppPixels[i][j];
						cout << ppPixels[i][j] << ' ';
					}
					cout << endl;
				}


				//���� � ���������
				cout << ">Processing..." << endl;
				result = myNetwork.readFunction(ppPixels);
				//result = myNetwork.readDataNoThreads(ppPixels);
				//����� ����������
				cout << ">Completed!" << endl;
				cout << result << endl;
				cout << ">Saving results..." << endl;
				fout << result << endl;
				cout << ">Results saved!" << endl;
			}
			fin.close();
			fout.close();
			cout << ">Files closed!" << endl;

		}
		else {
			cout << ">Files not open!" << endl;
		}
	}
	
	myNetwork.~neural_network();
}